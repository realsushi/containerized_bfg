# docker-bfg
## What is BFG?
Git repository stores codesources and versions usung the history.
BFG is a tool to recursively clean repository and history, because when a file is deleted, it remains in history. Modifiing each commit is tedious to do by hand and there is a risk of creating a divergent history.

## Abstract
Containerizes the
[BFG Repo-Cleaner](https://rtyley.github.io/bfg-repo-cleaner/) because I hate
running Java natively too.

## Usage SETUP with DOCKER 
### Check Docker is running
```s
git -v
docker -v
```

If Docker was installed just minutes ago, try with sudo. (or `sudo usermod -aG docker $USER` and reload shell)

### Clone repo or download Dockerfile
`git clone https://gitlab.com/-/ide/project/realsushi/containerized_bfg`

### Build the Container

```sh
docker run \
    --rm \
    -v $(readlink -m $(pwd)):/code \
    -w /code \
    tomislacker/bfg:1.14 # Then add your args here
```
### Run the container
In the example cloud_clusterk8s is a git repository.
```s
docker run    --rm    -v $PWD:/cloud_clusterk8s    -w /cloud_clusterk8s    docker-bfg --strip-blobs-bigger-than 49M cloud_clusterk8s.git 
```

### Known issue:
- BFG 1.14 is not compiled for ARM64 so it won't work on macos M1




##Old makefile
### Building Container

```sh
make container BFG_VERSION=1.14.0
```

### Easily Using the Container

```sh
make install BFG_VERSION=1.14.0
```

