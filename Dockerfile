FROM        openjdk:8-jre

ARG         BFG_VERSION=1.14.0

VOLUME      ['/code']

RUN         wget \
                --quiet \
                --output-document=/bfg.jar \
                https://repo1.maven.org/maven2/com/madgag/bfg/1.14.0/bfg-1.14.0.jar

# wip mod path for error get 501
            #http://repo1.maven.org/maven2/com/madgag/bfg/${BFG_VERSION}/bfg-${BFG_VERSION}.jar
COPY        assets/bfg /usr/bin/bfg
ENTRYPOINT  ["/usr/bin/bfg"]
CMD         ["--help"]
